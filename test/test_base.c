#include<stdio.h>
#include<stdlib.h>
#include<cl_simple/cl_simple.h>

int main(){

	char* inline_kernel = 
   	"#define __CL_SIMPLE_KERNEL_TEST_H 1\n"
	"#ifndef __CL_SIMPLE_KERNEL_INDICES_H\n"
	"	#include<cl_simple/kernels/k_indices.cl>\n"
	"#endif\n";

	char* ker_files[] = {inline_kernel, "k_test_base.cl","<k_test2_base.cl>"};

	clOverhead(ker_files,3);
	printf("initialised kernels\n");

	int i=0;
	int N=5;
	printf("calling poke2\n");
	callFunction("poke2",(void*[]) {&i},(size_t[]){N,N},2);

	float* A = SVMalloc(N*sizeof(float));
	unsigned long* ulA = SVMalloc(N*sizeof(unsigned long));
	float* B = SVMalloc(N*sizeof(float));
	float* C = SVMalloc(N*sizeof(float));

	printf("calling init\n");
	callFunction("init", (void*[]) {A, B, C, &N},(size_t[]){N},1);

	printf("calling sum\n");
	callFunction("sum", (void*[]) {A, B, C, &N},(size_t[]){N},1);

	for (int i=0; i<N; i++){
		printf("%f + %f = %f\n", A[i], B[i], C[i]);
	}

	printf("calling pokeVector on A and ulA\n");
	callFunction("pokeVector",(void*[]) {A, ulA},(size_t[]){N},1);

    printf("unsigned long vector from cpu\n");
	for (int i=0; i<N; i++){
		printf("%lu\n", ulA[i]);
	}

	SVMFree(A);
	SVMFree(ulA);
	SVMFree(B);
	SVMFree(C);
	return 0;
}
