__kernel void poke(){
	int i = get_global_id(0);
	printf("%i\n",i);
}

__kernel void poke2(int i){
	int j = get_global_id(i);
	printf("%i %i\n", i, j);
}


__kernel void pokeVector(
	__global    float* fvec,
	__global    unsigned long *ulvec){

	int i = get_global_id(0);
	printf("%f \t %lu\n",fvec[i],ulvec[i]);

}
