#define __CL_INTERACTIONS_H 1

#ifndef __CL_ALGEBRA_H
	#include<cl_simple/cl_algebra.h>
#endif

void hamiltonian(
	float*	R,
	float*	V,
	int		N,
	int		D,
	char*	type,
	float*	params,
	float*	F	){

	int wdims=2;
	size_t witems[] = {N,N};

	if (sameString(type, "lennard-jones"))
		callFunction("lennardJonesHamiltonian", (void*[]){R, V, &N, &D, params, F}, witems, wdims);
	else
		printf("Unknown hamiltonian type %s. Skipping calculation.\n", type);
}


void build_hamiltonian(
	float*	R,
	float*	V,
	int		N,
	int		D,
	char*	type,
	float*	params,
	float*	F	){

	int wdims=2;
	size_t witems[] = {N,N};

	if (sameString(type, "lennard-jones"))
		callFunction("lennardJonesEnergy", (void*[]){R, &N, &D, params, F}, witems, wdims);
	else
		printf("Unknown hamiltonian type %s. Skipping calculation.\n", type);
}


void bounded_pairs(
	float*	R,
	float*	V,
	int		N,
	int		D,
	char*	type,
	float*	params,
	float*	F	){

	int wdims=2;
	size_t witems[] = {N,N};

	if (sameString(type, "lennard-jones"))
		callFunction("lennardJonesBounded", (void*[]){R, V, &N, &D, params, F}, witems, wdims);
	else
		printf("Unknown hamiltonian type %s. Skipping calculation.\n", type);
}


void build_pairwise_forces(
	float*	R,
	float*	V,
	int		N,
	int		D,
	char*	type,
	float*	params,
	float*	F	){

	int wdims=2;
	size_t witems[] = {N,D*N};

	if (sameString(type, "lennard-jones"))
		callFunction("lennardJonesForces", (void*[]){R, &N, &D, params, F}, witems, wdims);
	else
		printf("Unknown interaction type %s. Skipping calculation.\n", type);
}


void constrain(
	float*	R,
	float*	V,
	int		N,
	int		D,
	char*	type,
	float*	params	){

	int wdims=2;
	size_t witems[] = {D,N};

	if (sameString(type, "reflective"))
		callFunction("reflectWalls", (void*[]){R, V, &N, &D, params}, witems, wdims);
	else
		printf("Unknown constraint type %s. Skipping calculation.\n", type);
}



void drift(
	float*	R,
	float*	V,
	int		N,
	int		D,
	float	dt	){
	
	int wdims=2;
	size_t witems[] = {D,N};

	callFunction("drift", (void*[]){R, V, &N, &D, &dt}, witems, wdims);
}


void kick(
	float*	R,
	float*	V,
	int		N,
	int		D,
	char*	type,
	float*	params,
	float	dt,
	float*	F	){

	build_pairwise_forces(R, V, N, D, type, params, F);
	sumCols(F, D*N, N, F);

	tens_op_scal(F, '*', dt, D*N, F);
	tens_op_tens(V, '+', F, D*N, V);
}


float* makegrid(int N, int D){

	float* R = SVMalloc(N*D*sizeof(float));
	int s = (int)pow(N, 1.0/D);
	s = (int)pow(s, D) < N ? s + 1 : s;

	for (int d=0; d<D; d++){
		init(&(R[d*N]), 'e', N);
		tens_op_scal(&(R[d*N]), 'd', pow(s, D - d - 1), N, &(R[d*N]));
		tens_op_scal(&(R[d*N]), '%', s, N, &(R[d*N]));
	}

	return R;
}
