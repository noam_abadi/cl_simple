#include<cl_simple/kernels/k_indices.cl>

__kernel void lennardJonesHamiltonian(
	__global	float*	R,
	__global	float*	V,
				int		N,
				int		D,
	__global	float*	params,
	__global	float*	H	){

	int ind = getInd();
	int i,j;
	int	skip = getSkip();

	float dummy, rij;
	float Vo = params[0];
	float ro = params[1];

	while (ind < N*N){
		i = ind%N;
		j = ind/N;

		dummy = 0.0;
		rij = 0.0;

		if (i!=j){

			for (int dim=0; dim<D; dim++){
				rij = R[i + dim*N] - R[j + dim*N];
				dummy += rij*rij;
			}
			dummy = ro*ro/dummy;
			dummy = 0.5*Vo*(dummy*dummy*dummy*dummy*dummy*dummy - 2*dummy*dummy*dummy);
			H[ind] = dummy;

		} else {
			for (int dim=0; dim<D; dim++)
				dummy += 0.5*V[i + dim*N]*V[j + dim*N];

			H[ind] = dummy;
		}

		ind += skip;
	}
}


__kernel void lennardJonesBounded(
	__global	float*	R,
	__global	float*	V,
				int		N,
				int		D,
	__global	float*	params,
	__global	float*	H	){

	int ind = getInd();
	int i,j;
	int	skip = getSkip();

	float dummy, rij;
	float Vo = params[0];
	float ro = params[1];

	while (ind < N*N){
		i = ind%N;
		j = ind/N;

		dummy = 0.0;
		rij = 0.0;

		if (i!=j){

			for (int dim=0; dim<D; dim++){
				rij = R[i + dim*N] - R[j + dim*N];
				dummy += rij*rij;
			}
			dummy = ro*ro/dummy;
			dummy = Vo*(dummy*dummy*dummy*dummy*dummy*dummy - 2*dummy*dummy*dummy);

			for (int dim=0; dim<D; dim++)
				dummy += 0.5*V[i + dim*N]*V[i + dim*N] + 0.5*V[j + dim*N]*V[j + dim*N];

			H[ind] = dummy < 0 ? 1 : 0;

		} else {
			H[ind] = 0;
		}

		ind += skip;
	}
}

__kernel void lennardJonesEnergy(
	__global	float*	R,
				int		N,
				int		D,
	__global	float*	params,
	__global	float*	H	){

	int ind = getInd();
	int i,j;
	int	skip = getSkip();

	float dummy, rij;
	float Vo = params[0];
	float ro = params[1];

	while (ind < N*N){
		i = ind%N;
		j = ind/N;

		dummy = 0.0;
		rij = 0.0;

		if (i!=j){

			for (int dim=0; dim<D; dim++){
				rij = R[i + dim*N] - R[j + dim*N];
				dummy += rij*rij;
			}
			dummy = ro*ro/dummy;
			dummy = 0.5*Vo*(dummy*dummy*dummy*dummy*dummy*dummy - 2*dummy*dummy*dummy);
			H[ind] = dummy;

		} else
			H[ind] = 0;

		ind += skip;
	}
}


__kernel void lennardJonesForces(
	__global	float*	R,
				int		N,
				int		D,
	__global	float*	params,
	__global	float*	F	){

	int ind = getInd();
	int i,j,d;
	int	skip = getSkip();

	float dummy, rij;
	float Vo = params[0];
	float ro = params[1];

	while (ind < D*N*N){

		i = ind%N;
		j = ind/(D*N);
		d = (ind%(D*N))/N;

		dummy = 0.0;
		rij = 0.0;

		if (i!=j){

			for (int dim=0; dim<D; dim++){
				rij = R[i + dim*N] - R[j + dim*N];
				dummy += rij*rij;
			}
			dummy = ro*ro/dummy;
			dummy = 12*Vo/(ro*ro)*(dummy*dummy*dummy*dummy*dummy*dummy*dummy - dummy*dummy*dummy*dummy);
			rij = (R[i + d*N] - R[j + d*N]);
			F[ind] = dummy*rij;

		} else
			F[ind] = 0;

		ind += skip;
	}
}


__kernel void drift(
	__global 	float*	R,
	__global	float*	V,
				int		N,
				int		D,
				float	dt	){

	int ind = getInd();
	int skip = getSkip();

	while (ind < D*N){
		R[ind] += V[ind]*dt;
		ind += skip;
	}
}


__kernel void reflectWalls(
	__global	float*	R,
	__global	float*	V,
				int		N,
				int		D,
	__global	float*	params	){

	int ind = getInd();
	int skip = getSkip();

	float vi, ri, side;
	int d;

	while (ind < D*N){

		ri = R[ind];
		vi = V[ind];
		d = ind/N;
		side = params[d];

		while (ri > side || ri < 0){
			ri = ri < 0 ? -ri : 2*side - ri;
			vi *= -1;
		}

		R[ind] = ri;
		V[ind] = vi;

		ind += skip;
	}
}
