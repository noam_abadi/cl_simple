#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>

#include"/home/noam/Documents/code/openCL/cl_simple/cl_simple.h"
#include"/home/noam/Documents/code/C/prettyprint.h"
#include"/home/noam/Documents/code/C/prettyplot.h"

#include"/home/noam/Documents/code/openCL/matrix_algebra/__algebra.h"
#include"/home/noam/Documents/code/openCL/random/cl_ran2.h"
#include"/home/noam/Documents/code/openCL/statistics/__statistics.h"
#include"/home/noam/Documents/code/openCL/cl_interactions/__interactions.h"

void printState(
	float	t,
	float*	R,
	char*	positions,
	float*	V,
	char*	velocities,
	float*	F,
	char*	energy,
	float*	LJ_params,
	int		N,
	int		D,
	int		header	){

	if (header){
		pHeader(positions, (char*[]) {"time", NULL}, 1, 's', 1, 0);
		pHeader(positions, (char*[]) {"position", NULL}, N, 'e', 0, 1);
	
		pHeader(velocities, (char*[]) {"time", NULL}, 1, 's', 1, 0);
		pHeader(velocities, (char*[]) {"velocity", NULL}, N, 'e', 0, 1);
	
		pHeader(energy, (char*[]) {"time", NULL}, 1, 's', 1, 0);
		pHeader(energy, (char*[]) {"potential energy", "kinetic energy", NULL}, N, 'e', 0, 1);
	}

	printWatermark(positions, &t, 1, 'R', 0);
	printVector(positions, R, N);

	printWatermark(velocities, &t, 1, 'R', 0);
	printVector(velocities, V, N);

	build_hamiltonian(R, V, N, D, "lennard-jones", LJ_params, F);
	sumCols(F, N, N, F);
	tens_op_tens(V, '*', V, N, &(F[D*N]));
	tens_op_scal(&(F[D*N]), '*', 0.5, N, &(F[D*N]));

	printWatermark(energy, &t, 1, 'R', 0);
	printVector(energy, F, 2*D*N);
}


int main(){

	int D = 2;
	int N = 100;
	float sep = 3.0;
	float Vm = 20;
	float dt = 0.0001;
	int NT = 500;

	float L = (float)((int)pow(N, 1.0/D));
	L = pow(L, D) < N ? L + 1 : L;
	L *= sep;

	char* energy = "energy_test.dat";
	char* positions = "position_test.dat";
	char* velocities = "velocity_test.dat";

	float Vo = 1.0;
	float ro = 1.0;

	char ker_inds[] = "/home/noam/Documents/code/openCL/cl_kernels/k_indices.cl";
	char ker_mats[] = "/home/noam/Documents/code/openCL/matrix_algebra/k_algebra.cl";
	char ker_rand[] = "/home/noam/Documents/code/openCL/random/k_ran2.cl";
	char ker_stat[] = "/home/noam/Documents/code/openCL/statistics/k_statistics.cl";
	char ker_intr[] = "/home/noam/Documents/code/openCL/cl_interactions/k_interactions.cl";

	clOverhead((char*[]) {ker_inds, ker_mats, ker_rand, ker_stat, ker_intr}, 5);
	cl_seed(time(NULL), D*N);

	float* R = makegrid(N, D);
	tens_op_scal(R, '+', 0.5, D*N, R);
	tens_op_scal(R, '*', sep, D*N, R);
	
	float* V = SVMalloc(N*D*sizeof(float));
	cl_ran2(V, D*N);
	tens_op_scal(V, '*', 2*Vm, D*N, V);
	tens_op_scal(V, '-', Vm, D*N, V);

	float* walls = SVMalloc(D*sizeof(float));
	for (int d=0; d<D; d++)
		walls[d] = L;

	float* LJ_params = SVMalloc(2*sizeof(float));
	LJ_params[0] = Vo;
	LJ_params[1] = ro;

	float* F = SVMalloc(D*N*N*sizeof(float));
	float pot, kin;

	//fprintf(gnuplot, "set xr[0:%f]\n", L);
	//fprintf(gnuplot, "set yr[0:%f]\n", L);
	//fprintf(gnuplot, "plot '-' u 1:2:3:4 with vectors head filled lt 2 notit\n");

	for (int i=0; i<NT; i++){
		drift(R, V, N, D, dt/2);
		kick(R, V, N, D, "lennard-jones", LJ_params, dt, F);
		constrain(R, V, N, D, "reflective", walls);
		drift(R, V, N, D, dt/2);		

		build_hamiltonian(R, V, N, D, "lennard-jones", LJ_params, F);
		sum(F, N*N, F);
		pot = F[0];

		tens_op_scal(V, 'p', 2, D*N, F);
		sum(F, D*N, F);
		kin = 0.5*F[0];

	}

	printf("# data, N, D=%i, %i\n",N, D);
	printMatrix(NULL, V, N, D);

	tens_op_scal(V, 'p', 2, D*N, F);
	sumAxis(F, N, D, 1, F);
	tens_op_scal(F, 'r', 2, N, F);
	int nbins = 30;
	Histogram(F, N, nbins, F);

	printf("# histogram results, nbins=%i\n",nbins);
	printMatrix(NULL, F, nbins, 2);

	simple_lineplot(F, &(F[nbins]), nbins, 1);

	printf("Starting ran2 teardown\n");
	cl_ran2Teardown();

	printf("Starting SVMFree\n");
	SVMFree(LJ_params);
	SVMFree(walls);
	SVMFree(R);
	SVMFree(V);
	SVMFree(F);
}
