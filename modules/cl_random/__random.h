#define CL_SIMPLE_RANDOM_H 1
#define CL_SIMPLE_RANDOM_PRECISION_DEFAULT float

#ifndef CL_SIMPLE_RANDOM_PRECISION
    #define CL_SIMPLE_RANDOM_PRECISION CL_SIMPLE_RANDOM_PRECISION_DEFAULT
#endif 
#ifndef CL_SIMPLE_ALGEBRA_H 1
    #include<cl_simple/__algebra.h>
#endif

#define INITIAL_VALUE_FOR_RANQ2_1 4101842887655102017LL
#define INITIAL_VALUE_FOR_RANQ2_2 1

unsigned long* CL_SEEDS;
unsigned long* CL_STATES1;
unsigned long* CL_STATES2;

void cl_seed(
	unsigned long	seedV,
	int				N_states	){

	CL_SEEDS = SVMalloc(N_states*sizeof(unsigned long));
	CL_STATES1 = SVMalloc(N_states*sizeof(unsigned long));
	CL_STATES2 = SVMalloc(N_states*sizeof(unsigned long));

	unsigned long init_ran2_1 = INITIAL_VALUE_FOR_RANQ2_1;
	unsigned long init_ran2_2 = INITIAL_VALUE_FOR_RANQ2_2;

	callFunction("setStartingStates", 
                (void*[])   {CL_STATES1, CL_STATES2, &init_ran2_1, &init_ran2_2, &N_states}, 
                (size_t[])  {N_states}, 1);

	callFunction("setSeeds",    (void*[])   {CL_SEEDS, &seedV, &N_states}, 
                                (size_t[])  {N_states}, 1);
}

void cl_ran2_dec(
	CL_SIMPLE_RANDOM_PRECISION* container,
	int		                    N_states    ){

	callFunction("shiftwseed",
                    (void*[])   {CL_STATES1, CL_SEEDS, &N_states}, 
                    (size_t[])  {N_states}, 1);

	callFunction("int64",
                    (void*[])   {CL_STATES1, CL_STATES2, &N_states, CL_STATES2}, 
                    (size_t[])  {N_states}, 1);

	callFunction("int64",
                    (void*[])   {CL_STATES1, CL_STATES2, &N_states, CL_STATES1}, 
                    (size_t[])  {N_states}, 1);

	callFunction("int64_decimal",
                    (void*[])   {CL_STATES1, CL_STATES2, &N_states, container}, 
                    (size_t[])  {N_states}, 1);
}


void cl_ran2_int(
    CL_SIMPLE_RANDOM_PRECISION  min_int,
    CL_SIMPLE_RANDOM_PRECISION  max_int,
	CL_SIMPLE_RANDOM_PRECISION* container,
	int		                    N_states    ){

	callFunction("shiftwseed",  
                    (void*[])   {CL_STATES1, CL_SEEDS, &N_states}, 
                    (size_t[])  {N_states}, 1);

	callFunction("int64",
                    (void*[])   {CL_STATES1, CL_STATES2, &N_states, CL_STATES2}, 
                    (size_t[])  {N_states}, 1);

	callFunction("int64",   
                    (void*[])   {CL_STATES1, CL_STATES2, &N_states, CL_STATES1}, 
                    (size_t[])  {N_states}, 1);

	callFunction("int64_intiger", 
                    (void*[])   {CL_STATES1, CL_STATES2, &min_int, &max_int, &N_states, container}, 
                    (size_t[])  {N_states}, 1);
}


void uniform(
    CL_SIMPLE_RANDOM_PRECISION  min,
    CL_SIMPLE_RANDOM_PRECISION  max,
    CL_SIMPLE_RANDOM_PRECISION* values,
    int                         N_states    ){

    cl_ran2_dec(values, N_states);
    tens_op_scal(values, '*', max-min, N_states, values);
    tens_op_scal(values, '+', min, N_states, values);    
}


void randint(
    CL_SIMPLE_RANDOM_PRECISION  min_int,
    CL_SIMPLE_RANDOM_PRECISION  max_int,
    CL_SIMPLE_RANDOM_PRECISION* values,
    int                         N_states    ){

    cl_ran2_int(min_int, max_int, values, N_states);
}


void cl_ran2Teardown(){

	SVMFree(CL_SEEDS);
	SVMFree(CL_STATES1);
	SVMFree(CL_STATES2);
}
