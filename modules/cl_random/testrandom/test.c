#include<stdlib.h>
#include<stdio.h>
#include<time.h>

//#include<cl_simple/__random.h>
#define CL_TARGET_OPENCL_VERSION 220
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#define CL_SIMPLE_RANDOM_PRECISION double
#define CL_SIMPLE_ALGEBRA_PRECISION double
#include<cl_simple/cl_simple.h>
#include<cl_simple/__algebra.h>
#include<cl_simple/__random.h>

//#define PRETTYPRINT_PRECISION DATATYPE
//#include"/home/noam/Documents/code/C/prettyprint.h"

void printVector(CL_SIMPLE_RANDOM_PRECISION* vector, int len){
    for (int i=0; i<len; i++){
        printf("%f\t",vector[i]);
    }
    printf("\n");
}

int main(){

	char* out = NULL;//"cl_ran_nums_N-100x100.dat";
	printf("# Initialising openCL\n");
    char defs[] =   "#define CL_SIMPLE_RANDOM_PRECISION double\n"
                    "#define CL_SIMPLE_ALGEBRA_PRECISION double\n\0";
	clOverhead((char*[]){defs, "<k_algebra.cl>", "<k_ran2.cl>"}, 3);
	int N = 1000;
	printf("# Setting seeds\n");

	cl_seed(time(NULL), N);

	CL_SIMPLE_RANDOM_PRECISION* A = SVMalloc(N*sizeof(CL_SIMPLE_RANDOM_PRECISION));

	printf("# Getting random numbers\n");
	for (int i=0; i<1000; i++){
		uniform(0, 1, A, N);
		printVector(A, N);
	}

	printf("# Teardown\n");
	SVMFree(A);
	cl_ran2Teardown();
	printf("# Success!\n");

	return 0;
}
