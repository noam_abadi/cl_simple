#include<cl_simple/kernels/k_algebra.cl>

__kernel void
create_histogram_matrix(
	__global	float*	A,
				int		N,
				float	minR,
				float	maxR,
				int		nbins,
	__global	float*	B	){

	int ind = getInd();
	int skip = getSkip();

	int i, j;
	float delta = (maxR - minR)/nbins;
	float val;

	while (ind < N*nbins){

		i = ind%nbins;
		j = ind/nbins;
		val = A[j];

		if ((i == 0) && (val < minR + delta))
			B[ind] = 1;
		else if ((minR + i*delta <= val) && (val < minR + (i+1)*delta) && (i > 0) && (i+1 < nbins))
			B[ind] = 1;
		else if ((i+1 == nbins) && (maxR - delta <= val))
			B[ind] = 1;
		else
			B[ind] = 0;

		ind += skip;
	}
}
