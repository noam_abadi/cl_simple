//Figure out dependencies between modules. When should I include what?
// This should be fixed

void moment(
	float*	A,
	int		N,
	int		m,
	float*	B	){

	float* C = B;
	if (A == B)
		C = SVMalloc(N*sizeof(float));

	tens_op_scal(A, 'p', (float)m, N, C);
	tens_op_scal(C, '/', (float)N, N, C);

	sumAxis(C, N, 1, 0, C);

	if (A == B){
		B[0] = C[0];
		SVMFree(C);
	}
}


void momentAxis(
	float*	A,
	int		rows,
	int		cols,
	int		axis,
	int		m,
	float*	B	){

	float* C = B;
	if (A == B)
		C = SVMalloc(rows*cols*sizeof(float));

	int shape[] = {rows, cols};

	tens_op_scal(A, 'p', (float)m, rows*cols, C);
	tens_op_scal(C, '/', (float)shape[axis], rows*cols, C);

	sumAxis(C, rows, cols, axis, C);

	if (A == B){

		int end = axis == 0 ? rows*cols : rows;
		int skip = axis == 0 ? rows : 1;

		Copy(C, 0, end, skip, B, 0, end, skip);
		SVMFree(C);
	}
}


void mean(
	float*	A,
	int		N,
	float*	B	){

	moment(A, N, 1, B);
}


void meanAxis(
	float*	A,
	int		rows,
	int		cols,
	int		axis,
	float*	B	){

	momentAxis(A, rows, cols, axis, 1, B);
}


void var(
	float*	A,
	int		N,
	float*	B	){

	float* C = B;
	if (A == B)
		C = SVMalloc(N*sizeof(float));

	mean(A, N, C);
	float m = C[0];
	moment(A, N, 2, C);
	C[0] -= m*m;

	if (A == B){
		B[0] = C[0];
		SVMFree(C);
	}
}


void varAxis(
	float*	A,
	int		rows,
	int		cols,
	int		axis,
	float*	B	){

	int shape[] = {rows, cols};

	float* v = SVMalloc(shape[!axis]*sizeof(float));

	float* C = B;
	if (A == B)
		C = SVMalloc(rows*cols*sizeof(float));

	meanAxis(A, rows, cols, axis, C);
	
	int end = axis == 1 ? rows : rows*cols;
	int skip = axis == 1 ? 1 : rows;

	Copy(C, 0, end, skip, v, 0, shape[!axis], 1);
	tens_op_tens(v, '*', v, shape[!axis], v);

	momentAxis(A, rows, cols, axis, 2, C);
	if (axis != 1) Copy(C, 0, rows*cols, rows, C, 0, cols, 1);
	tens_op_tens(C, '-', v, shape[!axis], v);

	if (A == B)
		SVMFree(C);

	Copy(v, 0, shape[!axis], 1, B, 0, end, skip);
	SVMFree(v);
}



void HistogramInRange(
	float*	A,
	int		N,
	float	minR,
	float	maxR,
	int		nbins,
	float*	B	){

	callFunction("create_histogram_matrix", (void*[]){A, &N, &minR, &maxR, &nbins, B}, (size_t[]){N, nbins}, 2);

	sumCols(B, nbins, N, B);
	Copy(B, 0, nbins, 1, B, nbins, 2*nbins, 1);
	init(B, 'e', nbins);
	tens_op_scal(B, '*', (maxR - minR)/nbins, nbins, B);
	tens_op_scal(B, '+', minR + (maxR - minR)/(2*nbins), nbins, B);
}


void Histogram(
	float*	A,
	int		N,
	int		nbins,
	float*	B	){

	float minR, maxR;
	min(A, N, B);
	minR = B[0];
	max(A, N, B);
	maxR = B[0];

	if ((maxR != INFINITY) && (minR != -INFINITY) && (maxR != -INFINITY) && (minR !=INFINITY) && !isnan(minR) && !isnan(maxR))
		HistogramInRange(A, N, minR, maxR, nbins, B);

	else
		printf("Automatically calculated minR = %f and maxR = %f cannot be used to generate a histogram. Provide range with HistogramInRange or filter out non-finite values.\n", minR, maxR);
}
