#include<stdlib.h>
#include<stdio.h>

//#include"/home/noam/Documents/code/C/prettyprint.h"
#include<cl_simple/cl_simple.h>
#include<cl_simple/__algebra.h>
#include<cl_simple/__statistics.h>

int main(){

	clOverhead((char*[]){"<k_algebra.cl>","<k_statistics.cl>"}, 2);

	int rows = 4;
	int cols = 3;

	int shape[] = {rows, cols};
	int axis = 1;

	float* A = SVMalloc(rows*cols*sizeof(float));
	float* B = SVMalloc(rows*cols*sizeof(float));

	init(A,'e', rows*cols);
	init(B,'0', rows*cols);

	char* out = NULL;

//	printHeader(out, (char*[]){"A matrix", NULL}, 1, 'e');
//	printMatrix(out, A, rows, cols);
//	printHeader(out, (char*[]){"B matrix", NULL}, 1, 'e');
//	printMatrix(out, B, rows, cols);

	meanAxis(A, rows, cols, 1, B);
//	printHeader(out, (char*[]){"Axis 1 mean", NULL}, 1, 'e');
//	printMatrix(out, B, rows, cols);
	varAxis(A, rows, cols, 1, B);
//	printHeader(out, (char*[]){"Axis 1 var", NULL}, 1, 'e');
//	printMatrix(out, B, rows, cols);

//	printHeader(out, (char*[]){"Histogram testing", NULL}, 1, 'e');
	int nsamp = 10000;
	int nbins = 100;

	float* rans = SVMalloc(nsamp*sizeof(float));
	float* ransc = SVMalloc(nsamp*sizeof(float));

	FILE* file = fopen("py_rand_num_N-10000.dat", "r");

	int i=0;
	while (fscanf(file, "%f", &(rans[i])) != EOF) i++;

	sum(rans, nsamp, ransc);

	out = "cl__statistics_N-100_t.dat";

//	printHeader(out, (char*[]){"Histogram", NULL}, 1, 'e');
	Histogram(rans, nsamp, nbins, ransc);
//	printMatrix(out, ransc, nbins, 2);

	SVMFree(rans);
	SVMFree(ransc);
	SVMFree(A);
	SVMFree(B);
}
