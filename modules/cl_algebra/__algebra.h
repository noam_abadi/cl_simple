#ifndef MATH_H
	#include<math.h>
	#define MATH_H 1
#endif
#define CL_SIMPLE_ALGEBRA_H 1
#define CL_SIMPLE_ALGEBRA_PRECISION_DEFAULT float

#ifndef CL_SIMPLE_ALGEBRA_PRECISION
	#define CL_SIMPLE_ALGEBRA_PRECISION CL_SIMPLE_ALGEBRA_PRECISION_DEFAULT
#endif

void init(
	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	char	method,
	int		size	){

	int wdims = 1;
	size_t witems[] = {size};

	callFunction("init", (void*[]) {A, &method, &size}, witems, wdims);
}


void Copy(
	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	int		sa,
	int		ea,
	int		ja,
	CL_SIMPLE_ALGEBRA_PRECISION*	B,
	int		sb,
	int		eb,
	int		jb	){

	//assert((ea - sa)/ja == (eb - sb)/jb);

	int wdims = 1;
	size_t witems[] = {(ea - sa)/ja};

	callFunction("copy", (void*[]){A, &sa,&ea,&ja, B, &sb,&eb,&jb}, witems, wdims);
}


void tens_op_scal(
	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	char	op,
	CL_SIMPLE_ALGEBRA_PRECISION	B,
	int		N,
	CL_SIMPLE_ALGEBRA_PRECISION*	C	){

	int wdims = 1;
	size_t witems[] = {N};

	callFunction("tens_op_scal", (void*[]){A, &op, &B, &N, C}, witems, wdims);
}


void tens_op_tens(
	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	char	op,
	CL_SIMPLE_ALGEBRA_PRECISION*	B,
	int		N,
	CL_SIMPLE_ALGEBRA_PRECISION*	C	){

	int wdims = 1;
	size_t witems[] = {N};

	callFunction("ew_tens_op_tens", (void*[]){A, &op, B, &N, C}, witems, wdims);
}


void outer_op_tens(
	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	int		N,
	char	op,
	CL_SIMPLE_ALGEBRA_PRECISION*	B,
	int		M,
	CL_SIMPLE_ALGEBRA_PRECISION*	C	){

	int wdims = 2;
	size_t witems[] = {N,M};

	callFunction("outer_tens_op_tens", (void*[]){A, &N, &op, B, &M, C}, witems, wdims);
}


void ew_matrix_op_vect(
	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	int		rows,
	int		cols,
	char	op,
	CL_SIMPLE_ALGEBRA_PRECISION*	B,
	int		axis,
	CL_SIMPLE_ALGEBRA_PRECISION*	C	){

	int wdims = 2;
	size_t witems[] = {rows, cols};

	callFunction("ew_matrix_vector", (void*[]){A, &rows, &cols, &op, B, &axis, C}, witems, wdims);
}


void opAxis(
	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	int		rows,
	int		cols,
	char	op,
	int		axis,
	CL_SIMPLE_ALGEBRA_PRECISION*	B	){

	int width = 0;
	int wdims = 2;
	size_t witems[] = {rows, cols};

	if (B != A)
		Copy(A, 0, rows*cols, 1, B, 0, rows*cols, 1);

	width = (int)witems[axis];
	while (width > 1){
		witems[axis] = width/2;
		callFunction("foldAxis", (void*[]){B, &rows, &cols, &width, &op, &axis, B}, witems, wdims);
		width = width/2 + width%2;
	}
}


void Binary(
	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	CL_SIMPLE_ALGEBRA_PRECISION	lower,
	CL_SIMPLE_ALGEBRA_PRECISION	upper,
	int		inv,
	CL_SIMPLE_ALGEBRA_PRECISION*	B,
	int		size	){

	CL_SIMPLE_ALGEBRA_PRECISION* C;

	if (A == B)
		C = SVMalloc(size*sizeof(CL_SIMPLE_ALGEBRA_PRECISION));
	else
		C = B;

	int wdims = 1;
	size_t witems[] = {size};

	callFunction("binary", (void*[]) {A, &lower, &upper, &inv, C, &size}, witems, wdims);
	
	if (A == B){
		Copy(C, 0, size, 1, A, 0, size, 1);
		SVMFree(C);
	}
}


void Transpose(
	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	int		rows,
	int		cols,
	CL_SIMPLE_ALGEBRA_PRECISION*	tA	){

	int wdims = 2;
	size_t witems[] = {rows, cols};

	callFunction("transpose", (void*[]) {A, &rows, &cols, tA}, witems, wdims);
}


void Tri(
	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	int		rows,
	int		cols,
	char	mode,
	CL_SIMPLE_ALGEBRA_PRECISION*	B	){

	int wdims = 2;
	size_t witems[] = {rows, cols};

	if (mode == 'u')
		callFunction("utri", (void*[]) {A, &rows, &cols, B}, witems, wdims);
	if (mode == 'l')
		callFunction("ltri", (void*[]) {A, &rows, &cols, B}, witems, wdims);
	if (mode == 'U')
		callFunction("utriD", (void*[]) {A, &rows, &cols, B}, witems, wdims);
	if (mode == 'L')
		callFunction("ltriD", (void*[]) {A, &rows, &cols, B}, witems, wdims);
	
}


void MatrixProduct(
	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	CL_SIMPLE_ALGEBRA_PRECISION*	B,
	int		rowsa,
	int		common,
	int		colsb,
	CL_SIMPLE_ALGEBRA_PRECISION*	C	){

	int wdims = 2;
	size_t witems[] = {rowsa, colsb};

	callFunction("matrix_product", (void*[]) {A, B, &rowsa, &common, &colsb, C}, witems, wdims);
}


void getDiagonal(
	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	int		rows,
	int		cols,
	CL_SIMPLE_ALGEBRA_PRECISION*	V	){

	int l = rows < cols ? rows : cols;
	Copy(A, 0, rows*cols, rows+1, V, 0, l, 1);
}


void setDiagonalWithVector(
	CL_SIMPLE_ALGEBRA_PRECISION*	V,
	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	int		rows,
	int		cols	){

	int l = rows < cols ? rows : cols;
	Copy(V, 0, l, 1, A, 0, rows*cols, rows+1);
}

void setDiagonalWithScalar(
	CL_SIMPLE_ALGEBRA_PRECISION	V,
	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	int		rows,
	int		cols	){

	int wdims = 1;
	size_t witems[] = {rows < cols ? rows : cols};
	callFunction("set_diagonal_from_scal", (void*[]) {&V, A, &rows, &cols}, witems, wdims);
}


void sumAxis(
	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	int		rows,
	int		cols,
	int		axis,
	CL_SIMPLE_ALGEBRA_PRECISION*	B	){

	opAxis(A, rows, cols, '+', axis, B);
}


void sumRows(
	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	int		rows,
	int		cols,
	CL_SIMPLE_ALGEBRA_PRECISION*	B	){

	sumAxis(A, rows, cols, 0, B);
}


void sumCols(
	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	int		rows,
	int		cols,
	CL_SIMPLE_ALGEBRA_PRECISION*	B	){

	sumAxis(A, rows, cols, 1, B);
}

void sum(
	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	int		N,
	CL_SIMPLE_ALGEBRA_PRECISION*	B	){

	sumRows(A, N, 1, B);
}


void max(
	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	int		N,
	CL_SIMPLE_ALGEBRA_PRECISION*	B	){

	opAxis(A, N, 1, 'M', 0, B);
}


void min(
	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	int		N,
	CL_SIMPLE_ALGEBRA_PRECISION*	B	){

	opAxis(A, N, 1, 'm', 0, B);
}
