#include<stdlib.h>
#include<stdio.h>

//#define CL_TARGET_OPENCL_VERSION 220
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#define CL_SIMPLE_ALGEBRA_PRECISION double
//#define PRETTYPRINT_PRECISION double

#include<cl_simple/cl_simple.h>
//#include"/home/noam/Documents/code/C/prettyprint.h"
#include<cl_simple/__algebra.h>

int main(){

	char* defs = "#define CL_SIMPLE_ALGEBRA_PRECISION double\n";
	clOverhead((char*[]){defs, "../kernels/k_algebra.cl"}, 2);

	int N = 5;

	CL_SIMPLE_ALGEBRA_PRECISION a = 2;
	CL_SIMPLE_ALGEBRA_PRECISION* A = SVMalloc(N*sizeof(CL_SIMPLE_ALGEBRA_PRECISION));
	CL_SIMPLE_ALGEBRA_PRECISION* B = SVMalloc(N*sizeof(CL_SIMPLE_ALGEBRA_PRECISION));
	CL_SIMPLE_ALGEBRA_PRECISION* C = SVMalloc(N*N*sizeof(CL_SIMPLE_ALGEBRA_PRECISION));

	init(A, 'e', N);
	init(B, '1', N);
	tens_op_scal(B, '*', (CL_SIMPLE_ALGEBRA_PRECISION)N, N, B);
	init(C, '0', N*N);

    printf("initialised tensors.\n");
	printf("A\n");
//	printMatrix(NULL, A, 1, N);
	printf("B\n");
//	printMatrix(NULL, B, 1, N);
	printf("C\n");
//	printMatrix(NULL, C, N, N);


	tens_op_scal(A,'+',a,N,C);
	tens_op_scal(B,'+',a,N,&(C[N]));
	tens_op_tens(A,'*',B,N,&(C[2*N]));

    printf("performed scalar and tensor operations.\n");

//	printHeader(NULL, (char*[]) {"col",NULL}, N, 'e');
//	printMatrix(NULL, C, N, N);

//	printHeader(NULL, (char*[]) {"0-axis sum",NULL}, 1, 'e');
//	printHeader(NULL, (char*[]) {"col",NULL}, N, 'e');

	opAxis(C, N, N, '+', 0, C);
//	printMatrix(NULL, C, N, N);

//	printHeader(NULL, (char*[]) {"Set diag of C to 1",NULL}, 1, 'e');
//	printHeader(NULL, (char*[]) {"col",NULL}, N, 'e');

	setDiagonalWithScalar(1, C, N, N);
//	printMatrix(NULL, C, N, N);

//	printHeader(NULL, (char*[]) {"Transpose",NULL}, 1, 'e');
//	printHeader(NULL, (char*[]) {"col",NULL}, N, 'e');

	Transpose(C, N, N, C);
//	printMatrix(NULL, C, N, N);

	SVMFree(A);
	SVMFree(B);
	SVMFree(C);
	return 0;
}
