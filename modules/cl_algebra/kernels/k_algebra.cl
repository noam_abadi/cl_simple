#define CL_SIMPLE_ALGEBRA_PRECISION_DEFAULT float
#ifndef CL_SIMPLE_ALGEBRA_PRECISION
	#define CL_SIMPLE_ALGEBRA_PRECISION CL_SIMPLE_ALGEBRA_PRECISION_DEFAULT
#endif

#ifndef __CL_SIMPLE_KERNEL_INDICES_H
	#include<cl_simple/kernels/k_indices.cl>
#endif


CL_SIMPLE_ALGEBRA_PRECISION operate(
	CL_SIMPLE_ALGEBRA_PRECISION	A,
	char	op,
	CL_SIMPLE_ALGEBRA_PRECISION	B	){

	// Binary arithmetic operations

	if (op == '+')
		return A + B;
	else if (op == '-')
		return A - B;
	else if (op == '*')
		return A * B;
	else if (op == '/')
		return A / B;
	else if (op == 'd')
		return (CL_SIMPLE_ALGEBRA_PRECISION)(((int)A) / ((int)B));
	else if (op == '%')
		return ((int)A) % ((int)B);
	else if (op == 'p')
		return (CL_SIMPLE_ALGEBRA_PRECISION)pow((CL_SIMPLE_ALGEBRA_PRECISION)A, (CL_SIMPLE_ALGEBRA_PRECISION)B);
	else if (op == 'r')
		return pow((CL_SIMPLE_ALGEBRA_PRECISION)A, (CL_SIMPLE_ALGEBRA_PRECISION)(1.0/B));
	else if (op == 'm')
		return A < B ? A : B;
	else if (op == 'M')
		return A > B ? A : B;
	else if (op == 'l'){
		if (B <= 0)
			return log(A);
		else
			return log(A)/log(B);
	}

	// Unary operations
	else if (op == 'a')
		return A > -A ? A : -A;
	else if (op == 'e')
		return (CL_SIMPLE_ALGEBRA_PRECISION)exp(A);

	// Binary logical operations

	else if (op == '<')
		return A < B ? 1 : 0;
	else if (op == '>')
		return A > B ? 1 : 0;
	else if (op == 'S')
		return A <= B ? 1 : 0;
	else if (op == 'G')
		return A >= B ? 1 : 0;
	else if (op == '=')
		return A == B ? 1 : 0;
	else if (op == '!')
		return A != B ? 1 : 0;
	else if (op == '&')
		return A && B ? 1 : 0;
	else if (op == '|')
		return A || B ? 1 : 0;
	else{
		printf("unrecognised operation %c\n",op);
		return -1;
	}
}

CL_SIMPLE_ALGEBRA_PRECISION getVal(
	char	meth,
	int		ind	){

	if (meth=='e')
		return ind;
	else if (meth == '0')
		return 0;
	else if (meth == '1')
		return 1;
	else{
		printf("No method %c known\n", meth);
		return -1;
	}
}


__kernel void init(
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	A,
				char	meth,
				int		size	){

	int ind = getInd();
	int skip = getSkip();

	while (ind < size){
		A[ind] = getVal(meth, ind);
		ind += skip;
	}
}


// N-tensor x 1-tensor -> N-tensor
__kernel void tens_op_scal(
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	A,
				char	op,
				CL_SIMPLE_ALGEBRA_PRECISION	B,
				int		N,
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	C	){

	int ind = getInd();
	int skip = getSkip();

	while (ind < N){
		C[ind] = operate(A[ind],op,B);
		ind += skip;
	}
}


// N-tensor x N-tensor -> N-tensor
__kernel void ew_tens_op_tens(
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	A,
				char	op,
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	B,
				int		N,
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	C	){

	int ind = getInd();
	int skip = getSkip();

	while (ind < N){
		C[ind] = operate(A[ind],op,B[ind]);
		ind += skip;
	}
}


// N-tensor x M-tensor -> NM-tensor
__kernel void outer_tens_op_tens(
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	A,
				int		N,
				char	op,
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	B,
				int		M,
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	C	){

	int ind = getInd();
	int skip = getSkip();

	int i, j;

	while (ind < N*M){
		i = ind%N;
		j = ind/N;
		C[ind] = operate(A[i],op,B[j]);
		ind += skip;
	}
}


__kernel void foldAxis(
    __global    CL_SIMPLE_ALGEBRA_PRECISION*  matrix,
                int     rows,
                int     cols,
                int     width,
                char    operator,
                int     axis,
    __global    CL_SIMPLE_ALGEBRA_PRECISION*  f_matrix	){

	int ind = getInd();
	int skip = getSkip();

    int indi, indj;

    int totalOps = axis == 0 ? cols : rows; // (cols*axis + rows*(1-axis))*width/2;
	totalOps *= (width/2);

    while (ind < totalOps){

		// Sum over different row vectors obtaining a row vector
        if (axis == 0){ 
            
            indi = ind%(width/2);
            indj = ind/(width/2);

            if(indi < width/2 && indj < cols)
                f_matrix[indi + indj*rows] = operate(matrix[indi + indj*rows], operator, matrix[width - 1 - indi + indj*rows]);

		// Sum over different column vectors obtaining a column vector
        } else if (axis == 1){
            indi = ind%rows;
            indj = ind/rows;

            if(indi < rows && indj < width/2)
                f_matrix[indi + indj*rows] = operate(matrix[indi + indj*rows], operator, matrix[indi + (width - 1 - indj)*rows]);

		}

        ind += skip;
    }
}

__kernel void ew_matrix_vector(
    __global    CL_SIMPLE_ALGEBRA_PRECISION*  A,
				int		rows,
				int		cols,
                char    op,
    __global    CL_SIMPLE_ALGEBRA_PRECISION*  B,
                int     axis,
    __global    CL_SIMPLE_ALGEBRA_PRECISION*  C	){

    int i = get_global_id(0);
    int j = get_global_id(1);
    int k = get_global_id(2);

    int gsi = get_global_size(0);
    int gsj = get_global_size(1);
    int gsk = get_global_size(2);

    int ind = i + j*gsi + k*gsi*gsj;

    int indi;
    int indj;
    
    while (ind < rows*cols){

        indi = ind%rows;
        indj = ind/rows;

        if (indi < rows && indj < cols){
            if (axis == 0)
                C[indi + indj*rows] = operate(A[indi + indj*rows], op, B[indi]);
            else if (axis == 1)
                C[indi + indj*rows] = operate(A[indi + indj*rows], op, B[indj]);
        }

        ind += gsi*gsj*gsk;
    }
}


__kernel void copy(
    __global    CL_SIMPLE_ALGEBRA_PRECISION*  source,
                int     ss,
                int     sf,
                int     sj,
    __global    CL_SIMPLE_ALGEBRA_PRECISION*  dest,
				int		ds,
				int		df,
				int		dj    ){

    int ind = getInd();
    int skip = getSkip();

    while (ind < ((sf - ss)/sj) || ind < ((df - ds)/dj)){
        dest[ds + ind*dj] = source[ss + ind*sj];
        ind += skip;
    }
}


__kernel void binary(
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	A,
				CL_SIMPLE_ALGEBRA_PRECISION	low,
				CL_SIMPLE_ALGEBRA_PRECISION	upp,
				int		inv,
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	B,
				int		size	){

	int ind = getInd();
	int skip = getSkip();

	while (ind < size){
		if (A[ind] >= low && A[ind] < upp)
			B[ind] = !inv;
		else
			B[ind] = inv;

		ind += skip;
	}
}


__kernel void transpose(
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	A,
				int		rows,
				int		cols,
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	B	){

	
	int ind = getInd();
	int skip = getSkip();

	int i, j;

	while (ind < rows*cols){
		i = ind%rows;
		j = ind/rows;

		B[j + i*cols] = A[i + j*rows];
		ind += skip;
	}

}


__kernel void ltri(
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	A,
				int		rows,
				int		cols,
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	B	){

	int ind = getInd();
	int skip = getSkip();

	int i, j;

	while (ind < rows*cols){
		i = ind%rows;
		j = ind/rows;

		if (j<i)
			B[i + j*rows] = A[i + j*rows];
		else
			B[i + j*rows] = 0;

		ind += skip;
	}
}


__kernel void ltriD(
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	A,
				int		rows,
				int		cols,
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	B	){

	int ind = getInd();
	int skip = getSkip();

	int i, j;

	while (ind < rows*cols){
		i = ind%rows;
		j = ind/rows;

		if (j<=i)
			B[i + j*rows] = A[i + j*rows];
		else
			B[i + j*rows] = 0;

		ind += skip;
	}
}


__kernel void utri(
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	A,
				int		rows,
				int		cols,
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	B	){

	int ind = getInd();
	int skip = getSkip();

	int i, j;

	while (ind < rows*cols){
		i = ind%rows;
		j = ind/rows;

		if (j>i)
			B[i + j*rows] = A[i + j*rows];
		else
			B[i + j*rows] = 0;

		ind += skip;
	}
}


__kernel void utriD(
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	A,
				int		rows,
				int		cols,
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	B	){

	int ind = getInd();
	int skip = getSkip();

	int i, j;

	while (ind < rows*cols){
		i = ind%rows;
		j = ind/rows;

		if (j>=i)
			B[i + j*rows] = A[i + j*rows];
		else
			B[i + j*rows] = 0;

		ind += skip;
	}
}


__kernel void matrix_product(
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	A,
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	B,
				int		rowsa,
				int		common,
				int		colsb,
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	C	){
	
	int ind = getInd();
	int skip = getSkip();

	int size = rowsa*colsb;

	int i, j;
	CL_SIMPLE_ALGEBRA_PRECISION dum;

	while (ind < size){
		i = ind%rowsa;
		j = ind/rowsa;

		dum = 0;
		for (int c=0; c<common; c++)
			dum += A[i + c*rowsa]*B[c + j*common];
		
		C[ind] = dum;
		ind += skip;
	}
}


__kernel void replace(
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	A,
				CL_SIMPLE_ALGEBRA_PRECISION	a,
				CL_SIMPLE_ALGEBRA_PRECISION	b,
				int		N,
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	B	){

	int ind = getInd();
	int skip = getSkip();

	while (ind < N){
		if (A[ind] == a)
			B[ind] = b;
		else
			B[ind] = A[ind];
		ind += skip;
	}
}

__kernel void set_diagonal_from_scal(
				CL_SIMPLE_ALGEBRA_PRECISION	V,
	__global	CL_SIMPLE_ALGEBRA_PRECISION*	A,
				int		rows,
				int		cols	){

	int ind = getInd();
	int skip = getSkip();

	int N = rows < cols ? rows : cols;

	while (ind*(1+rows) < rows*cols){
		A[ind*(1 + rows)] = V;
		ind += skip;
	}
}
