#ifndef CL_TARGET_OPENCL_VERSION
    #define CL_TARGET_OPENCL_VERSION 300
#endif
#ifndef __OPENCL_CL_H
    #include<CL/cl.h>
#endif

#ifndef __bool_true_false_are_defined
typedef enum bool { false, true } bool;
#endif

#define __CL_SIMPLE_H 1

#ifndef CL_SIMPLE_TARGET_VERSION
    #define CL_SIMPLE_TARGET_VERSION 10
#endif


#if CL_SIMPLE_TARGET_VERSION == 10
    #ifndef CL_KERNELS_DEFAULT_LOC
        #define CL_KERNELS_DEFAULT_LOC "/usr/local/include/cl_simple/kernels/"
    #endif

    #define CL_MEM_ALLOCATION_STYLE 0
    #define CL_SVM_ALLOCATION_STYLE 1
    #ifndef CL_ALLOCATION
        #define CL_ALLOCATION CL_SVM_ALLOCATION_STYLE
    #endif
#endif

#include<cl_simple/cl_env_independent.h>
#include<cl_simple/cl_env_dependent.h>
