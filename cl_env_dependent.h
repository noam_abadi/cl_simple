#ifndef CL_SIMPLE_ENV_INDEPENDENT_H
#include"./cl_simple1-0_cl_env_independent.h"
#endif

#define CL_SIMPLE_ENV_DEPENDENT_H 1

// Global variable so user doesn't have to deal with this.
cl_env env;

/*
All calls are such that user doesn't have to worry or know
about the environment variables.
*/


// Creates environment
void clOverhead (   char*   kerFileNames[],
                    int     numKerFiles ){

    clCreateEnvironment (&env, kerFileNames, numKerFiles);
}

/*
Allocate memory
*/
void* SVMalloc ( size_t  size ){ 
    return clSVMAlloc(env.context, CL_MEM_READ_WRITE, size, 0);
}

cl_mem clmemalloc ( size_t  size ){
    int err = 0;
    cl_mem obj = clCreateBuffer (env.context, CL_MEM_READ_WRITE, size, NULL, &err);
    if (err) printf("clmemalloc %i\n", err);
    return obj;
}

// Free memory
void SVMFree ( void* ptr ){
    clSVMFree(env.context, ptr);
}

void clmemFree ( cl_mem  ptr ){
    clReleaseMemObject(ptr);
}

/*
Write between host and device
(void* and cl_mem)
*/

int clDeviceToCPU ( cl_mem  mem_dev,
                    size_t  size,
                    void*   mem_cpu    ){

    return clEnqueueReadBuffer (env.cmd_q, mem_dev, CL_TRUE, 0, size,
                                mem_cpu, 0, NULL, NULL);
}

int clCPUToDevice ( void*   mem_cpu,
                    size_t  size,
                    cl_mem  mem_dev ){

    return clEnqueueWriteBuffer (env.cmd_q, mem_dev, CL_TRUE, 0, size,
                                 mem_cpu, 0, NULL, NULL);
}


void callFunction ( char*   func_name,
                    void*   arguments[],
                    size_t* num_work_items,
                    int     num_dims    ){

    bool nofunc = true;
	int ind;
	int start = getKey(func_name)%env.num_functions;

    for (int i=0; i<env.num_functions; i++){

		ind = (i+start)%env.num_functions;
        if (sameString(env.functions[ind].name, func_name)){
            clCallKernel(env, env.functions[ind].kernel, env.functions[ind].arg_types,
                         arguments, (const size_t*) num_work_items, num_dims);
            nofunc = false;
            break;
        }
    }

    if (nofunc){
        printf("No function %s has been initialised. Exiting\n", func_name);
        exit(-1);
    }
}
