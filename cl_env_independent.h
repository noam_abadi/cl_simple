#define CL_SIMPLE_ENV_INDEPENDENT_H 1

typedef void (*PTR_TO_NOTIFY)(const char*,const void*, size_t, void*);

typedef struct cl_function{

    cl_kernel   kernel;
    int         num_args;
    char**      arg_types;
    char*       name;

} cl_function;


/*
Structure includes simplest things needed to get an opencl envirionment running.
Plus functionspace to save all loaded kernels.
num_functions is actually function space size.
This should be fixed
*/
typedef struct cl_env{

    cl_platform_id                  PLATFORM;
    cl_device_id                    DEVICE;
    cl_context_properties*          CONTEXT_PROPS;
    PTR_TO_NOTIFY                   PFN_NOTIFY;
    void*                           USER_DATA;
    cl_context                      context;
    cl_command_queue_properties*    COMMAND_PROPS;
    cl_command_queue                cmd_q;
    char*                           COMPILE_OPTIONS;
    cl_program                      program;
    int                             num_functions;
    cl_function*                    functions;
    
} cl_env;


/* 
This getKey function (string to hash) is based on a suggestion I found on
Quora (should cite), which isn't that good for large hash tables but works
well for up to a few hundred items (seems like enough for now). 
*/

unsigned int getKey(char* word){
    int i = 0;
    int key = 5381;
    while (word[i] != '\0'){
        key = (key << 5) + key + word[i];
        i++;
    }   
    return key;
}


int sameString( char*   w1, 
                char*   w2  ){

	int i1 = 0;
	int i2 = 0;

	int same = 1;
	while (w1[i1] != '\0' || w2[i2] != '\0'){

		if (w1[i1] != w2[i2]){
			same = 0;
			break;
		}
		i1++;
		i2++;
	}

	return same;
}

int endsWith(   char*   word,
                char    symbol  ){

    int l=0;
    while (word[l] != '\0') l++;
    l--;
    if (word[l] == symbol)
        return 1;
    return 0;
}


FILE* clOpenSourceFromDefault ( char*   source_name,
                                char*   default_loc,
                                char*   mode ){

    FILE* source_file = NULL;

    int loc_name_size = 0;
    while (default_loc[loc_name_size] != '\0') loc_name_size++;
    int name_size = 0;
    while ((source_name[name_size] != '>') && (source_name[name_size] != '\0')) name_size++;

    char* full_name = malloc((loc_name_size + name_size + 1)*sizeof(char));

    int l=0;
    while (default_loc[l] != '\0'){
        full_name[l] = default_loc[l];
        l++;
    }
    l=1;    // To skip first '<'
    while (source_name[l] != '>' && source_name[l] != '\0'){  
        full_name[loc_name_size + l - 1] = source_name[l];
        l++;
    }

    if (source_name[l] == '>'){
        full_name[loc_name_size + l - 1] = '\0';
        source_file = fopen (full_name, mode);

    } else {
        printf("Incorrect kernel naming for default location. Use <%s>. Aborting.\n", source_name);
        exit(-1);
    }

    free (full_name);
    return source_file;
}
    


char* clMakeSourceWithDefaultPath ( cl_env* penv,
                                    char*   source_name,
                                    char*   default_loc ){

    size_t source_size = 0;
	FILE* source_file = NULL;
    char* source_buff = NULL;

    // To emulate #include style:
    if (source_name[0] == '<')
        source_file = clOpenSourceFromDefault (source_name, default_loc, "r");

    else 
        source_file = fopen (source_name, "r");

	if (source_file != NULL){
		fseek (source_file, 0, SEEK_END);
		source_size = ftell (source_file);
		rewind (source_file);
		source_buff = (char*) malloc ((source_size + 1)*sizeof(char));
		source_buff[source_size] = '\0';
		fread (source_buff, sizeof(char), source_size, source_file);
		fclose (source_file);

	} else {
		// To allow char* in the source file as kernels (or kernel modifiers)
		int err;
		cl_program prog = clCreateProgramWithSource (penv->context, 1, 
			(const char*) &source_name, NULL, &err);

		if (!err)
			return source_name;

		printf ("Couldn't open %s. Aborting.\n", source_name);
		exit (-1);
	}

    return source_buff;
}


int clCompileProgramWithOptions (   cl_env* penv,
                                    char*   options ){

	penv->COMPILE_OPTIONS = options;
	int err = clBuildProgram (penv->program, 1, (const cl_device_id*) &(penv->DEVICE),
                          penv->COMPILE_OPTIONS, (void (*)(cl_program, void*)) penv->PFN_NOTIFY, 
                          penv->USER_DATA);
    return err;
}


void clDisplayCompileError (cl_env* penv){

	size_t log_size;
	clGetProgramBuildInfo (penv->program, penv->DEVICE, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
	char* log = (char*) malloc (log_size*sizeof(char));
	clGetProgramBuildInfo (penv->program, penv->DEVICE, CL_PROGRAM_BUILD_LOG, log_size, log, NULL);
	printf("%s\n", log);
}


int clHashKernelNames (cl_env* penv){

    size_t name_list_size;
    int err;
    int num_kernels = penv->num_functions;

    err = clGetProgramInfo (penv->program, CL_PROGRAM_KERNEL_NAMES, 
                            0, NULL, &name_list_size);
    char* name_list = (char*) malloc (name_list_size*sizeof(char));
    err = clGetProgramInfo (penv->program, CL_PROGRAM_KERNEL_NAMES, 
                            name_list_size, name_list, NULL);
    if (err) printf ("Kernel names %i\n", err);

    for (int i=0; i<num_kernels; i++)
        penv->functions[i].name = "-1";

    int l=0;
    int s=0;
    int k, ko;
    for (int i=0; i<num_kernels; i++){

        while (name_list[l] != ';' && name_list[l] != '\0') l++;
        char* fname = (char*) malloc ((l-s+1)*sizeof(char));

        l = s;
        while (name_list[l] != ';' && name_list[l] != '\0'){
            fname[l-s] = name_list[l];
            l++;
        }
        fname[l-s] = '\0';
        
        k = getKey (fname)%num_kernels;
        if (sameString (penv->functions[k].name, "-1"))
            penv->functions[k].name = fname;

        else{
            ko = k;
            k++;
            while ((!sameString(penv->functions[k].name, "-1")) && (k-ko))
                k = (k+1)%num_kernels;

            if (k-ko) 
                penv->functions[k].name = fname;
            else printf("Everywhere is full. This shouldn't have happened.\n");
        }

        l++;
        s = l;
    }

    free (name_list);
    return err;
}


int clBuildKernels (cl_env* penv){

    int err;
    int num_kernels = penv->num_functions;
    size_t arg_type_size;
    cl_function* func;

    for (int i=0; i<num_kernels; i++){
        func = &(penv->functions[i]);
        func->kernel = clCreateKernel (penv->program, func->name, &err);
        err = clGetKernelInfo (func->kernel, CL_KERNEL_NUM_ARGS, 
                                sizeof(cl_uint), &(func->num_args), NULL);
        if (err) printf("Number of arguments for kernel %s %i\n", func->name, err);

        func->arg_types = (char**) malloc ((func->num_args + 1)*sizeof(char*));

        int arg_ind = 0;
        for (arg_ind=0; arg_ind<(func->num_args); arg_ind++){

            err = clGetKernelArgInfo (func->kernel, arg_ind, CL_KERNEL_ARG_TYPE_NAME, 
                                        0, NULL, &arg_type_size);
            func->arg_types[arg_ind] = malloc (arg_type_size*sizeof(char));
            err = clGetKernelArgInfo (func->kernel, arg_ind, CL_KERNEL_ARG_TYPE_NAME,
                                        arg_type_size, func->arg_types[arg_ind], NULL);

            if (err) printf ("Argument type for kernel %s arg %i %i\n", func->name, 
                            arg_ind, err);
        }
        func->arg_types[arg_ind] = "EOA";
    }

    return err;
}


/*
Compile kernel_file_name file.
On success, add kernels to function space.
On faliure, print log.
*/
void clMakeKernelsFromFiles(    cl_env*	penv,
	                            char*	kernel_file_names[],
                                int     num_files   ){

    int err = 0;

    char** progBuffs = (char**) malloc(num_files*sizeof(char*));
    for (int i=0; i<num_files; i++)
        progBuffs[i] = clMakeSourceWithDefaultPath (penv, kernel_file_names[i], CL_KERNELS_DEFAULT_LOC);

	penv->program = clCreateProgramWithSource (penv->context, num_files, (const char**) progBuffs, NULL, &err);
	if(err) printf("Program %d\n", err);

    char options[] = "-cl-kernel-arg-info";
    err = clCompileProgramWithOptions (penv, options);
	if(err) printf("Compile %d\n", err);

	if(err == CL_BUILD_PROGRAM_FAILURE) clDisplayCompileError (penv);

	else {

        size_t num_kernels;
        err = clGetProgramInfo (penv->program, CL_PROGRAM_NUM_KERNELS, 
                                sizeof(size_t), &num_kernels, NULL);
        if (err) printf("Number of kernels %i\n", err);

        if (penv->num_functions == 0){

            penv->num_functions = num_kernels;
            penv->functions = (cl_function*) malloc (num_kernels*sizeof(cl_function));
            err = clHashKernelNames (penv);
            if (err) printf ("Hash kernel names to function space %i\n", err);

            err = clBuildKernels (penv);
            if (err) printf ("Build kernel functions %i\n", err);

        } else {
            printf("Unavailable option for now.\n");
            printf("Please specify all kernels in cl_create_env (clOverhead).\n");
            exit(-1);
        }
	}
}


/*
Fill in cl_env structure with opencl envirionment (defined in opencl API)
with the minimum needed to run something on first available platform and
las available GPU (why? it just happened).
Platform and device selection method is stupid. Should be fixed.
*/

void clCreateEnvironment(   cl_env* penv,
                            char*	kernel_file_names[],
	                        int		num_kernel_file_names ){

	int num_plat, num_dev, err;

	err = clGetPlatformIDs (0, NULL, &num_plat);
	cl_platform_id* plats = malloc(num_plat*sizeof(cl_platform_id));
    err = clGetPlatformIDs (num_plat, plats, NULL);

	if(err) printf("Platform %i\n", err);

	if (num_plat > 1)
		printf ("%i platforms available. Choosing first with GPU compatibilities\n", num_plat);

	for (int i=0; i<num_plat; i++){

		err = clGetDeviceIDs (plats[i], CL_DEVICE_TYPE_GPU, 0, NULL, &num_dev);
		if(!err && num_dev){

            cl_device_id* devs = malloc(num_dev*sizeof(cl_device_id));
	        err = clGetDeviceIDs (plats[i], CL_DEVICE_TYPE_GPU, num_dev, devs, NULL);
			if (err) printf("Device %i", err);

			if (num_dev > 1)
				printf ("%i devices available. Choosing last\n", num_dev);

			penv->DEVICE = devs[num_dev - 1];
			penv->PLATFORM = plats[i];
            num_dev = 1;
            free(plats);
            free(devs);
			break;

		} else if (err != CL_DEVICE_NOT_FOUND)
			printf("Device %i", err);
		
	}

	penv->CONTEXT_PROPS = NULL;
	penv->PFN_NOTIFY = NULL;
	penv->USER_DATA = NULL;

	penv->context = clCreateContext (penv->CONTEXT_PROPS, num_dev, (const cl_device_id*) &(penv->DEVICE), 
                                     penv->PFN_NOTIFY, penv->USER_DATA, &err);
	if(err) printf("Context %d\n", err);

	penv->COMMAND_PROPS = NULL;
    penv->cmd_q = clCreateCommandQueueWithProperties (penv->context, penv->DEVICE, 
                         (const cl_command_queue_properties*) penv->COMMAND_PROPS, &err);

	if(err) printf("Command queue %d\n", err);

    penv->num_functions = 0;
    penv->functions = NULL;

	if (kernel_file_names)
	    clMakeKernelsFromFiles (penv, kernel_file_names, num_kernel_file_names);
}

/*
Interpret argument types and arguments for calling kernels easily.
Array-types are identified by opencl api as (e.g.) float*, so I 
detect them as ending in *. How can I allow user to use either cl_mem
or SVM arguments (when available?). For now, this is chosen by the
CL_ALLOCATION variable in preprocessor definitions from cl_simple1-0.h,
but this should be fixed.
*/

int clSetKernelArgument (   cl_kernel   func,
                            int         arg_ind,
                            char*       type,
                            void*       argument    ){

    int err;
    if (sameString(type, "float")){
        //float* parg = (float*) argument;
        //float arg = *parg;
        err = clSetKernelArg (func, arg_ind, sizeof(float), (const void*) argument);

    } else if (sameString(type, "int")){
        //int* parg = (int*) argument;
        //int arg = *parg;
        err = clSetKernelArg (func, arg_ind, sizeof(int), (const void*) argument);

    } else if (sameString(type, "double")){
        //int* parg = (int*) argument;
        //int arg = *parg;
        err = clSetKernelArg (func, arg_ind, sizeof(double), (const void*) argument);
            
    } else if (sameString(type, "ulong")){
        //unsigned long* parg = (unsigned long*) argument;
        //unsigned long arg = *parg;
        err = clSetKernelArg (func, arg_ind, sizeof(unsigned long), (const void*) argument);

    } else if (sameString(type, "bool")){
        //bool* parg = (bool*) argument;
        //bool arg = *parg;
        err = clSetKernelArg (func, arg_ind, sizeof(bool), (const void*) argument);

    } else if (sameString(type, "char")){
        //char* parg = (char*) argument;
        //char arg = *parg;
        err = clSetKernelArg (func, arg_ind, sizeof(char), (const void*) argument);

    } else if (endsWith(type, '*')){

        if (CL_ALLOCATION == CL_MEM_ALLOCATION_STYLE){
            //cl_mem* parg = (cl_mem*) argument;
            //cl_mem arg = *parg;
            //err = clSetKernelArg (func, arg_ind, sizeof(arg), (const void*) parg);
            err = clSetKernelArg (func, arg_ind, sizeof(cl_mem), (const void*) argument);

        } else if (CL_ALLOCATION == CL_SVM_ALLOCATION_STYLE) {
            err = clSetKernelArgSVMPointer(func, arg_ind, (const void*) argument);
        } else {
            printf("Allocation style error\n");
            exit(-1);
        }

    } else {
        printf("Invalid argument type %s. Stopping run.\n", type);
        exit(-1);
    }

    return err;
}

/*
Call a kernel from the function space without waiting for it to terminate.
*/

int clCallKernelNoWait( cl_env          env,
                        cl_kernel       func,
                        char*           types[],
                        void*           arguments[], 
                        const size_t*   num_nodes,
                        int             num_dim ){

    int err = 0;
    if( types && arguments ){

        int i=0;
        while(!sameString(types[i], "EOA")){
            err = clSetKernelArgument (func, i, types[i], arguments[i]);
            i++;
        }
    }
    if( err ) printf("set args %d\n",err);  

    err = clEnqueueNDRangeKernel (env.cmd_q, func, num_dim, NULL, num_nodes, NULL, 0, NULL, NULL);
    if( err ) printf("Enque %d\n", err);

    return err;    
}


/*
Call a kernel from the function space. Returns when the execution has finished.
*/

int clCallKernel(   cl_env          env,
                    cl_kernel       func,
                    char*           types[],
                    void*           arguments[],
                    const size_t*   num_nodes,
                    int             num_dim ){

    int err = clCallKernelNoWait(env, func, types, arguments, num_nodes, num_dim);

    err = clFinish(env.cmd_q);
    if( err ) printf("Wait for cmd_q %d\n", err);

    return err;    
}
