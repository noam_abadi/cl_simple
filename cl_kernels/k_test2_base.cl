//int getInd(){
//
//	int i = get_global_id(0);
//	int j = get_global_id(1);
//	int k = get_global_id(2);
//
//	int gsi = get_global_size(0);
//	int gsj = get_global_size(1);
//	int gsk = get_global_size(2);
//
//	return i+j*gsi+k*gsi*gsj;
//}
//
//int getSkip(){
//	int gsi = get_global_size(0);
//	int gsj = get_global_size(1);
//	int gsk = get_global_size(2);
//
//	return gsi*gsj*gsk;
//}

__kernel void init(
	__global float* A,
	__global float* B,
	__global float* C,
			 int	N	){

	int i = getInd();
	int s = getSkip();
	while (i < N){
		A[i] = i;
		B[i] = N;
		C[i] = 0;
		i += s;
	}
}

__kernel void sum(
	__global float* A,
	__global float* B,
	__global float* C,
			 int	N	){

	int i = getInd();
	int s = getSkip();
	while (i < N){
		C[i] = B[i] + A[i];
		i += s;
	}
}
