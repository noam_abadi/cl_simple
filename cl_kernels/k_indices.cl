#define __CL_SIMPLE_KERNEL_INDICES_H 1

int getInd(){

	int i=get_global_id(0);
	int j=get_global_id(1);
	int k=get_global_id(2);

	int gsi=get_global_size(0);
	int gsj=get_global_size(1);
	int gsk=get_global_size(2);
	
	return i + j*gsi + k*gsi*gsj;
}


int getSkip(){

	int gsi=get_global_size(0);
	int gsj=get_global_size(1);
	int gsk=get_global_size(2);
	
	return gsi*gsj*gsk;
}
