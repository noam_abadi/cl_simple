#define CL_SIMPLE_RANDOM_PRECISION_DEFAULT float
#ifndef CL_SIMPLE_RANDOM_PRECISION
    #define CL_SIMPLE_RANDOM_PRECISION CL_SIMPLE_RANDOM_PRECISION_DEFAULT
#endif

#ifndef __CL_SIMPLE_KERNEL_INDICES_H
    #include<cl_simple/kernels/k_indices.cl>
#endif

#ifndef __CL_SIMPLE_KERNEL_ALGEBRA_H
    #include<cl_simple/kernels/k_algebra.cl>
#endif

__kernel void setSeeds(
	__global unsigned long* CL_SEEDS,
			 unsigned long	seed,
					  int	nstates	){

	int ind = getInd();
	int skip = getSkip();

	while (ind < nstates){

		CL_SEEDS[ind] = seed + (unsigned long) ind;
		
		ind += skip;
	}
}


__kernel void setStartingStates(
	__global unsigned long*	CL_STATES1,
	__global unsigned long* CL_STATES2,
			 unsigned long	init1,
			 unsigned long	init2,
			 		  int	nstates	){

	int ind = getInd();
	int skip = getSkip();
	
	while (ind < nstates){

		CL_STATES1[ind] = init1;
		CL_STATES2[ind] = init2;
		
		ind += skip;
	}
}

__kernel void shiftwseed(
	__global unsigned long*	CL_STATES,
	__global unsigned long* CL_SEEDS,
			 		  int	nstates	){

	int ind = getInd();
	int skip = getSkip();
	
	while (ind < nstates){

		CL_STATES[ind] ^= CL_SEEDS[ind];
		
		ind += skip;
	}
}


unsigned long shiftState1(unsigned long stt1){
    stt1 ^= stt1 >> 17;
    stt1 ^= stt1 << 31;
    stt1 ^= stt1 >> 8;

    return stt1;
}

unsigned long shiftState2(unsigned long stt2){
    return 4294957665U*(stt2 & 0xffffffff) + (stt2 >> 32);
}

unsigned long mixStates(
    unsigned long   stt1,
    unsigned long   stt2    ){

    return stt1 ^ stt2;
}

CL_SIMPLE_RANDOM_PRECISION mixStates_dec(
    unsigned long   stt1,
    unsigned long   stt2    ){

    return (CL_SIMPLE_RANDOM_PRECISION)(5.42101086242752217E-20*mixStates(stt1, stt2));
}


CL_SIMPLE_RANDOM_PRECISION mixStates_int(
    unsigned long   stt1,
    unsigned long   stt2    ){

    return (CL_SIMPLE_RANDOM_PRECISION)(5.42101086242752217E-20*mixStates(stt1, stt2));
}

__kernel void int64(
	__global unsigned long*	CL_STATES1,
	__global unsigned long* CL_STATES2,
			 		  int	nstates,
	__global unsigned long* CL_STATES3	){

	int ind = getInd();
	int skip = getSkip();

	unsigned long stt1, stt2;
	
	while (ind < nstates){

		stt1 = shiftState1(CL_STATES1[ind]);
		stt2 = shiftState2(CL_STATES2[ind]);		

		CL_STATES1[ind] = stt1;
		CL_STATES2[ind] = stt2;
		CL_STATES3[ind] = mixStates(stt1, stt2);

		ind += skip;
	}
}


__kernel void int64_decimal(
	__global    unsigned long*		        CL_STATES1,
	__global    unsigned long*		        CL_STATES2,
			    int		                    nstates,
	__global    CL_SIMPLE_RANDOM_PRECISION* container	){

	int ind = getInd();
	int skip = getSkip();

	unsigned long stt1, stt2;
	
	while (ind < nstates){

		stt1 = shiftState1(CL_STATES1[ind]);
		stt2 = shiftState2(CL_STATES2[ind]);
		
		CL_STATES1[ind] = stt1;
		CL_STATES2[ind] = stt2;
		container[ind] = mixStates_dec(stt1, stt2);

		ind += skip;
	}
}


__kernel void int64_intiger(
	__global    unsigned long*		        CL_STATES1,
	__global    unsigned long*		        CL_STATES2,
                CL_SIMPLE_RANDOM_PRECISION  min_int,
                CL_SIMPLE_RANDOM_PRECISION  max_int,
			    int		                    nstates,
	__global    CL_SIMPLE_RANDOM_PRECISION* container	){

	int ind = getInd();
	int skip = getSkip();

	unsigned long stt1, stt2;
    int n = (int)(max_int - min_int);
	
	while (ind < nstates){

		stt1 = shiftState1(CL_STATES1[ind]);
		stt2 = shiftState2(CL_STATES2[ind]);
		
		CL_STATES1[ind] = stt1;
		CL_STATES2[ind] = stt2;
		container[ind] = (CL_SIMPLE_RANDOM_PRECISION)((int)min_int + (int)(mixStates_dec(stt1, stt2)*n));

		ind += skip;
	}
}
